Progetto_gestione_permessi

db: tabelle utente , permessi

la tabella utente ha momentaneamente un campo "ruolo" in cui è possibile settare il valore "responsabile", come stringa per momentanea semplicità di utilizzo
sarebbe meglio associarlo ad una tabella che contiene una lista di ruoli
la tabella permesso consente di registrare sia i permessi riguardanti le ferie sia le ore di permesso, lasciando a null i campi 
che non devono essere compilati a seconda della scelta del tipo di permesso

potrebbe essere più ordinato creare 2 tabelle differenti ma forse si rischierebbe una gestione più macchinosa, 


pagina login 


per testare il progetto creare il database progetto poi in automatico le tabelle con "ring.jpa.hibernate.ddl-auto = create" e inserire un utente

** aggiunta funzionalità di registrazione **

è possibile registrare un utente e  di conseguenza il suo account direttamente da interfaccia grafica senza bisogno di utilizzare pgAdmin;
viene cosi ovviato il problema di inserire una password cifrata nel database manualmente

**da sviluppare tutti i controlli al momento della registrazione**---- completato
come ad esempio duplicazione di username, la conferma della password, la duplicazione di un indirizzo e-mail ecc..

invio Mail:
utilizzando JavaMail ho creato un account di appoggio su Gmail da cui verranno inviate tutte le richieste; registrare e gestire gli accessi dinamicamente di tutti gli account di posta per ogni utente mi risulta abbastanza pesante al momento(nonostante debba essere la cosa giusta da fare);

il destinatario delle rischieste è attualmente il mio indirizzo email, per questioni di test, ma basterebbe ricavare l'email utente in base al responsabile scelto quando si effettua la richiesta; valore impostabile nelle classi richiestaFerieController e richiestaPermessiController.

**in fase di sviluppo funzionalità di accettazione dei permessi**----- completato

**altro:**
per gli utenti che come ruolo hanno "responsabile" sto pensando di fa comparire una notifica nella home page del gestionale quando vengono rilevate delle richieste a loro riferite; in modo da invitare il responsabile ad accedere alla sua area personale quando riceve la mail per approvare o meno le richieste, evitando di inserire i due link nella stessa.


**funzionalità accettazione permessi**
nella home page del gestionale viene visualizzato un button il cui valore varia a seconad del ruolo di chi ha effettuato l'accesso:
1)*area responsabili* :per coloro che non sono responsabili, l'accesso infatti non viene consentito.
2)*hai N nuove richieste*: l'area è accessibile per coloro che ricoprono il ruolo di responsabili, inoltre viene visualizzato il numero di richieste in attesa.
procedimento: al momento del login la pagina loginController assegna il valore alla variabile num_richieste, che altro non è che la stringa che verrà visualizzata sul button. nel settare questa variabile la funzione invoca nella pagina rchiestaService una funzione che prima controlla che l'account loggato sia responsabile e in caso positivo conta il numero di permessi che corrispondono al suo codice e sono "in attesa", altrimenti restituisce la stringa "Area responsabili";

nell'area responsabili le richieste vengono visualizzate in un dataTable, per ognuna i pulsanti CONCEDI e NEGA che richiamano un Dialog che dovrà contenere i dati relativi alla riga in cui vengono selezionati, per fare ciò ho dovuto utilizzare una classe SelezioneController per poter "intercettare" in un nuovo oggetto la richiesta selezionata.
in seguito nella stessa pagina vengono anche gestiti gli update delle richieste, modificando il valore di "concesso" da in attesa a 'si' o 'no'; l'invio dell'email di avviso all'utente e il redirect alla stessa pagina, con la visualizzazione di un'alert che comunica che l'operazione è andata a buon fine.


**cambiamenti**
sono stati modificati alcuni nomi dei campi delle tabelle (in particolare gli id) perche ho rilevato qualche problema con i nomi che contenevano il "_" che è stato eliminato 




