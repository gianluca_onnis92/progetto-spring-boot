package it.progetto.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.progetto.domain.Utente;

public interface UtenteRepository extends CrudRepository<Utente, Long> {
	
	List<Utente> findByUsername(String username);

	List<Utente> findByRuolo(String ruolo);

	Utente findIdByUsername(String username);

	Utente findEmailByUsername(String name);

	Utente findOneByUsername(String username);

	List<Utente> findByEmail(String email);

	
	
	

	Utente findIdByUsernameAndRuolo(String name, String ruolo);

	Utente findOneByUsernameAndRuolo(String name, String ruolo);

	Utente findEmailByIdutente(Long id);
		
	
	
}
