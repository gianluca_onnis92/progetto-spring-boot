package it.progetto.repository;


import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.progetto.domain.Permesso;

public interface PermessoRepository extends CrudRepository<Permesso, Long > {

	List<Permesso> findByUtente(Long u);

	List<Permesso> findByUtenteAndConcesso(Long u, boolean b);

	List<Permesso> findByUtenteAndTipo(Long u, boolean b);

	List<Permesso> findByUtenteAndTipoAndConcesso(Long u, boolean b, String c);

	ArrayList<Permesso> findAllByResponsabile(String s);

	ArrayList<Permesso> findAllByResponsabile(Long resp);

	ArrayList<Permesso> findAllByResponsabileAndConcesso(String s, String c);

	Permesso findOneByIdpermesso(Long id);

	

	

	
}
