package it.progetto.domain;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="permessi")
public class Permesso {


	
	private Long idpermesso;
	private String motivo;
	private boolean tipo; //Ferie(false) o permesso(true)
	
	//per ferie
	private Date giorno_inizio;
	private Date giorno_fine;
	
	//per permessi
	private Date data_permesso;
	private double ora_inizio;
	private double ora_rientro;
	
	private String responsabile;
	private String concesso; // permesso negato(0) permesso concesso (1)
	private Long utente;  //PK di "utente"
	
	@Id
	@Column(name="idpermesso")
	@GeneratedValue(strategy =GenerationType.SEQUENCE)
	public Long getIdpermesso() {
		return idpermesso;
	}
	public void setIdpermesso(Long idpermesso) {
		this.idpermesso = idpermesso;
	}
	
	@Column(name="motivo")
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	@Column(name = "f_p")
	public boolean isTipo() {
		return tipo;
	}
	public void setTipo(boolean tipo) {
		this.tipo = tipo;
	}
	
	@Column(name="giorno_inizio")
	public Date getGiorno_inizio() {
		return giorno_inizio;
	}
	public void setGiorno_inizio(Date giorno_inizio) {
		this.giorno_inizio = giorno_inizio;
	}
	
	@Column(name="giorno_fine")
	public Date getGiorno_fine() {
		return giorno_fine;
	}
	public void setGiorno_fine(Date giorno_fine) {
		this.giorno_fine = giorno_fine;
	}
	
	@Column(name="data_permesso")
	public Date getData_permesso() {
		return data_permesso;
	}
	public void setData_permesso(Date data) {
		this.data_permesso = data;
	}
	
	@Column(name="ora_inizio")
	public double getOra_inizio() {
		return ora_inizio;
	}
	public void setOra_inizio(double ora_inizio2) {
		this.ora_inizio = ora_inizio2;
	}
	
	@Column(name="ora_rientro")
	public double getOra_rientro() {
		return ora_rientro;
	}
	public void setOra_rientro(double ora_rientro2) {
		this.ora_rientro = ora_rientro2;
	}
	
	@Column(name="responsabile")
	public String getResponsabile() {
		return responsabile;
	}
	public void setResponsabile(String responsabile) {
		this.responsabile = responsabile;
	}
	

	@Column(name="concesso")
	public String getConcesso() {
		return concesso;
	}
	public void setConcesso(String concesso) {
		this.concesso = concesso;
	}
	@Column(name="utente")
	public Long getUtente() {
		return utente;
	}
	public void setUtente(Long utente) {
		this.utente = utente;
	}
	
	
	
	
}
