package it.progetto.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="utente")
public class Utente  {
		
	private Long idutente;
	private String nome;
	private String cognome;
	private String email;
	private String username;
	private String password;
	private String ruolo;
	
	public Utente() {
		
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy =GenerationType.SEQUENCE)
	public Long getIdutente() {
		return idutente;
	}
	public void setIdutente(Long idutente) {
		this.idutente = idutente;
	}
	
	@Column(name="nome")
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Column(name="cognome")
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	
	@Column(name="username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name="password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="ruolo")
	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	
	
}



/*
problemi in avvio con annotation @Entity(javax.persistence.Entity)

output:
Exception encountered during context initialization - cancelling refresh attempt: org.springframework.beans.factory.BeanCreationException: 
Error creating bean with name 'entityManagerFactory' defined in class path resource [org/springframework/boot/autoconfigure/orm/jpa/HibernateJpaAutoConfiguration.class]: 
Invocation of init method failed; nested exception is org.hibernate.AnnotationException: No identifier specified for entity: it.java.model.utente

Error starting ApplicationContext.

dependecy mancante?

*/
