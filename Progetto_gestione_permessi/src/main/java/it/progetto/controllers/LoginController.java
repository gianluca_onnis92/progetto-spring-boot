package it.progetto.controllers;



import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;


import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;

import org.springframework.security.core.context.SecurityContextHolder;

import it.progetto.service.RichiestaService;

@ManagedBean(name="loginController")
@RequestScoped
public class LoginController {
	
	
    private String username = null;
    private String password = null;
    public String num_richieste = null;
    
    @ManagedProperty(value="#{authenticationManager}")
    private AuthenticationManager authenticationManager = null;
    
    @ManagedProperty(value="#{richiestaService}")
    private RichiestaService richiestaService;
  
	
    private Long idConnesso;
    
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}
	
	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	
	
	public String getUtenteConnesso() {
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();
	
		return name;
	}
	
	public String getNum_richieste() {
		if (this.num_richieste == null) {
			this.num_richieste = richiestaService.richieste();	
		}
		return num_richieste;
	}
	
	public String AreaResp() {
		if(richiestaService.isResponsabile()) {
			return "/secure/area_responsabili.xhtml?faces-redirect=true";
		}
		return "layoutPrivato.xhtml?faces-redirect=true&resp=1";
	}

	public RichiestaService getRichiestaService() {
		return richiestaService;
	}

	public void setRichiestaService(RichiestaService richiestaService) {
		this.richiestaService = richiestaService;
	}
	
	

	
	
	
	
}
