package it.progetto.controllers;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.mail.MessagingException;
import javax.swing.JOptionPane;

import it.progetto.domain.Permesso;


import it.progetto.service.RichiestaService;
import it.progetto.service.SendMailService;
import net.bootsfaces.component.alert.Alert;
import net.bootsfaces.component.alert.AlertRenderer;

@ManagedBean(name="richiestaPermessiController")
@ViewScoped
public class RichiestaPermessiController {
	


	private RichiestaForm richiestaForm = new RichiestaForm();
	
	@ManagedProperty(value = "#{richiestaService}")	
    public RichiestaService richiestaService;  


	@ManagedProperty(value = "#{sendMailService}")
    public SendMailService sendMailService;  
	
	public String registraPermesso() throws MessagingException {
		
		if (paginaValida() == false) {
			return null;
		}else {
			final double min_i =(richiestaForm.getMinuti_i()/100);
			final double ora_inizio = richiestaForm.getOra_inizio()+min_i;
			final double min_r =(richiestaForm.getMinuti_r()/100);
			final double ora_rientro= richiestaForm.getOra_rientro()+min_r;
		// si salva nel db il nuovo permesso
		Permesso permesso = new Permesso();
		permesso.setMotivo(richiestaForm.getMotivo());
		permesso.setTipo(true);
		permesso.setGiorno_inizio(richiestaForm.getData_inizio());
		permesso.setGiorno_fine(richiestaForm.getData_rientro());
		permesso.setOra_inizio(ora_inizio);
		permesso.setOra_rientro(ora_rientro);
		permesso.setData_permesso(richiestaForm.getData_permesso());
		
		
		permesso.setUtente(richiestaService.IdUtenteConnesso());
		permesso.setResponsabile(richiestaForm.getResponsabile());
		permesso.setConcesso("in attesa");
		
	
		
		richiestaService.registraPermesso(permesso);
		
		//procedura di creazione e invio mail
		String to = "gianluca_onnis@hotmail.it";
		String subject ="Richiesta permesso";
		String body ="<h3> Alla cortese attenzione del Responsabile: " + richiestaForm.getResponsabile() 
						+ "</h3><h4>Si comunica che il dipendente : "+richiestaService.UtenteConnesso().getNome() +" " +richiestaService.UtenteConnesso().getCognome()
						+ "</h4>Ha presentato una richiesta di permesso giornaliero, da attuarsi in data  <b> "	+" "+ richiestaForm.getData_permesso() 
					
						+ "</b><br/>a partire dalle ore :<b>  " + "  " + richiestaForm.getOra_inizio() +':'+ richiestaForm.getMinuti_i()+" "						
						+ "</b>  sino alle : " + "<b> " + richiestaForm.getOra_rientro()+':'+ richiestaForm.getMinuti_r()
						
						+ "</b><br/> <br/> Accedi alla tua area personale per esaminare la richiesta ";
								
			sendMailService.inviaEmail(to, subject , body );
		
		return "/template/conferma.xhtml?faces-redirect=true";
		}
	}
		public void verifica_ora_inizio() {
			int oraInizio = richiestaForm.getOra_inizio();
			if(oraInizio < 9) {
				FacesContext.getCurrentInstance().addMessage("richiesta:inizio", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "l'ora di inizio non può essere inferiore delle 9:00 "));				
			}else if(oraInizio >= 18) {
				FacesContext.getCurrentInstance().addMessage("richiesta:inizio", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "l'ora di inizio non può essere superiore alle 17:45 "));				
			}
		}
		
		public void verifica_ora_rientro() {
			int oraInizio = richiestaForm.getOra_inizio();
			int oraRientro = richiestaForm.getOra_rientro();
			if(oraRientro <= oraInizio) {
				FacesContext.getCurrentInstance().addMessage("richiesta:rientro", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "l'ora di rientro non può essere inferiore o uguale a quella di uscita"));				
			}else if(oraRientro >= 18) {
				FacesContext.getCurrentInstance().addMessage("richiesta:inizio", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "l'ora di rientro non può essere superiore alle 17:45 "));				
			}
		}
		public boolean paginaValida()  {
			int oraInizio = richiestaForm.getOra_inizio();
			int oraRientro = richiestaForm.getOra_rientro();
			if(oraInizio < 9) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "l'ora di inizio non può essere inferiore delle 9:00");
	            context.addMessage("richiesta:inizio", message);
	            return false;
			}
			if(oraInizio >= 18) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "l'ora di inizio non può essere superiore alle 17:45");
	            context.addMessage("richiesta:inizio", message);
	            return false;
			}
			if(oraRientro < oraInizio) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "l'ora di rientro non può essere inferiore a quella di uscita");
	            context.addMessage("richiesta:inizio", message);
	            return false;
			}
			if(oraRientro >=18) {
				FacesContext context = FacesContext.getCurrentInstance();
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "l'ora di rientro non può essere superiore alle 17:45 ");
	            context.addMessage("richiesta:inizio", message);
	            return false;
			}
			
			
				return true;
			
			
		}
	
		

	public RichiestaForm getRichiestaForm() {
		return richiestaForm;
	}



	public void setRichiestaForm(RichiestaForm richiestaForm) {
		this.richiestaForm = richiestaForm;
	}



	public RichiestaService getRichiestaService() {
		return richiestaService;
	}



	public void setRichiestaService(RichiestaService richiestaService) {
		this.richiestaService = richiestaService;
	}



	public SendMailService getSendMailService() {
		return sendMailService;
	}



	public void setSendMailService(SendMailService sendMailService) {
		this.sendMailService = sendMailService;
	}

	
	
}
