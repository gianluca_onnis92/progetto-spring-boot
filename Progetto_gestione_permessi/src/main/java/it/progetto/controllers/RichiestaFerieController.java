package it.progetto.controllers;


import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.mail.MessagingException;
import it.progetto.domain.Permesso;
import it.progetto.service.RichiestaService;
import it.progetto.service.SendMailService;



@ManagedBean(name="richiestaFerieController")
@ViewScoped
public class RichiestaFerieController {
	
	private RichiestaForm richiestaForm = new RichiestaForm();
	
	@ManagedProperty(value = "#{richiestaService}")
    public RichiestaService richiestaService;  

	@ManagedProperty(value = "#{sendMailService}")
    public SendMailService sendMailService;  

		
	public String registraPermesso() throws MessagingException {
		if(verifica_date() == false) {
			return "/secure/richiesta_f.xhtml?faces-redirect=true&date_sbagliate=1";
		}else {
		
		// si salva nel db il nuovo permesso
		Permesso permesso = new Permesso();
		permesso.setMotivo(richiestaForm.getMotivo());
		permesso.setTipo(false);
		permesso.setGiorno_inizio(richiestaForm.getData_inizio());
		permesso.setGiorno_fine(richiestaForm.getData_rientro());
		permesso.setOra_inizio(richiestaForm.getOra_inizio());
		permesso.setOra_rientro(richiestaForm.getOra_rientro());
		permesso.setData_permesso(richiestaForm.getData_permesso());
		
		
		permesso.setUtente(richiestaService.IdUtenteConnesso());
		permesso.setResponsabile(richiestaForm.getResponsabile());
		permesso.setConcesso("in attesa");
		
	
		
		richiestaService.registraPermesso(permesso);
		
		
		//procedura di creazione e invio mail

		String to = "gianluca_onnis@hotmail.it";
		String subject ="Richiesta ferie";
		String body ="<h3> Alla cortese attenzione del Responsabile: " + richiestaForm.getResponsabile() 
						+ "<br/>Si comunica che il dipendente  "+richiestaService.UtenteConnesso().getNome() +" " +richiestaService.UtenteConnesso().getCognome()
						+ "</h3><br/>Ha presentato una richiesta di ferie, a partire dal giorno:  <b> "	+" "+ richiestaForm.getData_inizio() 
					
						+ "</b><br/><br/>con rientro previsto per il:<b>  " + " "+richiestaForm.getData_rientro() 
						
						
						+ "</b><br/> <br/>per la seguente motivazione: " + richiestaForm.getMotivo()
						
						+ "<br/> <br/>Accedi alla tua area personale per esaminare la richiesta ";
	
			sendMailService.inviaEmail(to, subject , body );
		
		
		return "/template/conferma.xhtml?faces-redirect=true";
		//notifica all'utente che la richiesta è stata completata
		}
	}
	public boolean verifica_date() {
		Date data1 = richiestaForm.getData_inizio();
		Date data2 = richiestaForm.getData_rientro();
		if (data2.before(data1)) {
			return false;
		}else {
			return true;
		}
	}
	
	public void sendMail() throws MessagingException  {
				
	}

	public RichiestaForm getRichiestaForm() {
		return richiestaForm;
	}



	public void setRichiestaForm(RichiestaForm richiestaForm) {
		this.richiestaForm = richiestaForm;
	}



	public RichiestaService getRichiestaService() {
		return richiestaService;
	}



	public void setRichiestaService(RichiestaService richiestaService) {
		this.richiestaService = richiestaService;
	}

	public SendMailService getSendMailService() {
		return sendMailService;
	}

	public void setSendMailService(SendMailService sendMailService) {
		this.sendMailService = sendMailService;
	}
	
	
	
}
