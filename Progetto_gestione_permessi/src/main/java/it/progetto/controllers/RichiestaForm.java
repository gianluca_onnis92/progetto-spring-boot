package it.progetto.controllers;

import java.util.Date;

public class RichiestaForm {
	
	private String motivo;
	private boolean F_P;
	private Date giorno_inizio;
	private Date giorno_fine;
	private int ora_inizio;
	private int minuti_i;
	private int minuti_r;
	private int ora_rientro;
	private Date data_permesso;
	private Long utente;
	private String responsabile;
	private boolean concesso=false;
	
	
	
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public boolean getF_P() {
		return F_P;
	}
	public void setF_P(boolean f_P) {
		F_P = f_P;
	}
	public Date getData_inizio() {
		return giorno_inizio;
	}
	public void setData_inizio(Date data_inizio) {
		this.giorno_inizio = data_inizio;
	}
	public Date getData_rientro() {
		return giorno_fine;
	}
	public void setData_rientro(Date data_rientro) {
		this.giorno_fine = data_rientro;
	}
	public int getOra_inizio() {
		return ora_inizio;
	}
	public void setOra_inizio(int ora_inizio) {
		this.ora_inizio = ora_inizio;
	}
	public int getOra_rientro() {
		return ora_rientro;
	}
	public void setOra_rientro(int ora_rientro) {
		this.ora_rientro = ora_rientro;
	}
	public Date getData_permesso() {
		return data_permesso;
	}
	public void setData_permesso(Date data_permesso) {
		this.data_permesso = data_permesso;
	}
	public Long getUtente() {
		return utente;
	}
	public void setUtente(Long utente) {
		this.utente = utente;
	}
	public String getResponsabile() {
		return responsabile;
	}
	public void setResponsabile(String responsabile) {
		this.responsabile = responsabile;
	}
	public boolean isConcesso() {
		return concesso;
	}
	public void setConcesso(boolean concesso) {
		this.concesso = concesso;
	}
	public int getMinuti_i() {
		return minuti_i;
	}
	public void setMinuti_i(int minuti_i) {
		this.minuti_i = minuti_i;
	}
	public int getMinuti_r() {
		return minuti_r;
	}
	public void setMinuti_r(int minuti_r) {
		this.minuti_r = minuti_r;
	}
	
	
	
	
	
	

}
