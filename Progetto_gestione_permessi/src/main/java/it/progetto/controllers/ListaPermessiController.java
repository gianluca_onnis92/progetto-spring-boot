package it.progetto.controllers;

import java.util.ArrayList;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import it.progetto.domain.Permesso;


import it.progetto.service.RichiestaService;

@ManagedBean
@ViewScoped
public class ListaPermessiController {
	
	private ArrayList<Permesso> ListaPermessi;
	private ArrayList<Permesso> ListaPermessiUtente;
	private ArrayList<Permesso> ListaPermessiConcessi;
	private ArrayList<Permesso> ListaFerieUtente;
	private ArrayList<Permesso> ListaFerieConcesse;
	private ArrayList<Permesso> ListaRichieste;
	
	@ManagedProperty(value = "#{richiestaService}")
    private RichiestaService richiestaService;
	

	
	public ArrayList<Permesso> getlistaPermessi(){		
		if (this.ListaPermessi == null) {
            this.ListaPermessi = richiestaService.listaPermessi();
        }
        return ListaPermessi;	
	}
	
	public ArrayList<Permesso> getListaPermessiUtente(){		
		if (this.ListaPermessiUtente == null) {
            this.ListaPermessiUtente = richiestaService.listaPermessiUtente();
        }
        return ListaPermessiUtente;	
	}
	public ArrayList<Permesso> getlistaPermessiConcessi(){		
		if (this.ListaPermessiConcessi == null) {
            this.ListaPermessiConcessi = richiestaService.listaPermessiConcessi();
        }
        return ListaPermessiConcessi;	
	}

	public ArrayList<Permesso> getlistaFerieUtente(){		
		if (this.ListaFerieUtente == null) {
            this.ListaFerieUtente = richiestaService.listaFerieUtente();
        }
        return ListaFerieUtente;	
	}


	public ArrayList<Permesso> getlistaFerieConcesse(){		
		if (this.ListaFerieConcesse == null) {
            this.ListaFerieConcesse = richiestaService.listaFerieConcesse();
        }
        return ListaFerieConcesse;	
	}
	
	public ArrayList<Permesso> getListaRichieste(){
		if (this.ListaRichieste == null) {
            this.ListaRichieste = richiestaService.richiesteRicevute();
        }
        return ListaRichieste;	
	}
	
	public RichiestaService getRichiestaService() {
		return richiestaService;
	}



	public void setRichiestaService(RichiestaService richiestaService) {
		this.richiestaService = richiestaService;
	}
	
}
