package it.progetto.controllers;

import java.util.ArrayList;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;


import it.progetto.domain.Utente;
import it.progetto.service.RichiestaService;

@ManagedBean
@ViewScoped
public class ListaResponsabiliController {

	
	private ArrayList<Utente> listaResponsabili;
	
	private Long idConnesso;
	
	@ManagedProperty(value = "#{richiestaService}")
    private RichiestaService richiestaService;
	
	public ArrayList<Utente> getlistaResponsabili(){		
		if (this.listaResponsabili == null) {
            this.listaResponsabili = richiestaService.listaResponsabili();
        }
        return listaResponsabili;	
	}

	

	public RichiestaService getRichiestaService() {
		return richiestaService;
	}

	public void setRichiestaService(RichiestaService richiestaService) {
		this.richiestaService = richiestaService;
	}
	public Long getIdConnesso() {
		
		this.idConnesso=richiestaService.IdUtenteConnesso();
		
		return idConnesso;
		
	}
	
}
