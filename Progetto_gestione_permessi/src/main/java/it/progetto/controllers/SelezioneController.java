package it.progetto.controllers;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.mail.MessagingException;

import it.progetto.domain.Permesso;
import it.progetto.domain.Utente;
import it.progetto.service.PermessoService;
import it.progetto.service.RichiestaService;
import it.progetto.service.SendMailService;

@ManagedBean(name = "selezioneController")
@ViewScoped
public class SelezioneController {

	
	private Permesso permessoSelected;
	private ArrayList<Utente> listaUtenti;
	
	@ManagedProperty(value = "#{permessoService}")
	public PermessoService permessoService;

	@ManagedProperty(value = "#{sendMailService}")
    public SendMailService sendMailService;  

	@ManagedProperty(value = "#{richiestaService}")
    public RichiestaService richiestaService; 
	
	
	
	public String concediPermesso() throws MessagingException {
		Long id= permessoSelected.getIdpermesso();
		Permesso permesso = permessoService.trovaPermessoById_permesso(id);
			
		permesso.setConcesso("si");
		
		String to = richiestaService.email(permessoSelected.getUtente());
		//String to = "gianluca_onnis@hotmail.it";
		String subject ="Approvazione Richiesta";
		String body ="<h3> Si comunica che il Responsabile  "+ richiestaService.UtenteConnesso().getNome() + " " +richiestaService.UtenteConnesso().getCognome()
						+ "</h3>Ha valutato la tua richiesta, che è stata approvata;"
						
						+ " <br/><br/>Distinti Saluti, <br/> Il responsabile : "+ richiestaService.UtenteConnesso().getNome() + " " +richiestaService.UtenteConnesso().getCognome() ; 
						
	
			sendMailService.inviaEmail(to, subject , body );
		
		permessoService.salvaPermesso(permesso);
		
		return "/secure/area_responsabili.xhtml?faces-redirect=true&concesso=1";
	}

	
	public String negaPermesso() throws MessagingException {
		Long id= permessoSelected.getIdpermesso();
		Permesso permesso = permessoService.trovaPermessoById_permesso(id);
		
	
		permesso.setConcesso("no");
		
		
		String to = "gianluca_onnis@hotmail.it";
		String subject ="Approvazione Richiesta";
		String body ="<h3> Si comunica che il Responsabile  "+ richiestaService.UtenteConnesso().getNome() + " " +richiestaService.UtenteConnesso().getCognome()
						+ "</h3>Ha valutato la tua richiesta, e per ovvie ragioni ha deciso di non accettarla;"
						+ "<br />ci scusiamo per l'inconveniente, per eventuali domande rivolgersi al responsabile incaricato<br/>"
						+ " <br/><br/>Distinti Saluti, <br/> Il responsabile : "+ richiestaService.UtenteConnesso().getNome() + " " +richiestaService.UtenteConnesso().getCognome() ; 
						
	
			sendMailService.inviaEmail(to, subject , body );
		
		permessoService.salvaPermesso(permesso);
		
		return "/secure/area_responsabili.xhtml?faces-redirect=true&negato=1";
		
	}
	
	public ArrayList<Utente> getListaUtenti(){
		if(this.listaUtenti == null) {
			this.listaUtenti = richiestaService.listaUtenti();
		}
		return listaUtenti;
	}
	
	
	public PermessoService getPermessoService() {
		return permessoService;
	}

	public void setPermessoService(PermessoService permessoService) {
		this.permessoService = permessoService;
	}

	public Permesso getPermessoSelected() {
		return permessoSelected;
	}

	public void setPermessoSelected(Permesso permessoSelected) {
		this.permessoSelected = permessoSelected;
	}


	public SendMailService getSendMailService() {
		return sendMailService;
	}


	public void setSendMailService(SendMailService sendMailService) {
		this.sendMailService = sendMailService;
	}


	public RichiestaService getRichiestaService() {
		return richiestaService;
	}


	public void setRichiestaService(RichiestaService richiestaService) {
		this.richiestaService = richiestaService;
	}
	
	
	
	
}
