package it.progetto.controllers;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import it.progetto.domain.Utente;
import it.progetto.service.RegistrazioneService;

@ManagedBean(name="registrazioneController")
@ViewScoped
public class RegistrazioneController {

	
	private RegistrazioneForm registrazioneForm = new RegistrazioneForm();
	
	
	@ManagedProperty(value ="#{registrazioneService}")
	public RegistrazioneService registrazioneService;

	public String registraUtente() {
		if(paginaValida() == false) {
			return null;
		}else {

			// si salva nel db il nuovo utente
			Utente utente = new Utente();
			
			utente.setNome(registrazioneForm.getNome());
			utente.setCognome(registrazioneForm.getCognome());
			utente.setUsername(registrazioneForm.getUsername());
			utente.setPassword(new BCryptPasswordEncoder().encode(registrazioneForm.getPassword()));
			utente.setEmail(registrazioneForm.getEmail());
			utente.setRuolo(registrazioneForm.getRuolo());
			
			
			
			registrazioneService.registraUtente(utente);
					
			
			return "conferma.xhtml?faces-redirect=true";
		}
		
		}
	
		public boolean verificaMail() {
		String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
		
		Pattern pattern = Pattern.compile(regex);
		String email = registrazioneForm.getEmail();
		Matcher matcher = pattern.matcher(email);
		
		boolean mailValida = matcher.matches();
		
		if (!mailValida) {
			FacesContext.getCurrentInstance().addMessage("registrazione:email", 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "L'email inserita non è valida"));
		}
		
		return mailValida;
	}
		
		public boolean emailPresente() {
			String email= registrazioneForm.getEmail();
			boolean presente = registrazioneService.emailPresente(email);
			if(presente) {
						FacesContext.getCurrentInstance().addMessage("registrazione:email", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "L'email inserita non è valida"));
					}
				return presente;
			}
		public boolean usernamePresente() {
			String username= registrazioneForm.getUsername();
			boolean presente = registrazioneService.usernamePresente(username);
			if(presente) {
						FacesContext.getCurrentInstance().addMessage("registrazione:username", 
						new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "L'username inserito è già stato utilizzato"));
					}
				return presente;
			}
		public boolean verificaRuolo() {
			
				if (registrazioneForm.getRuolo() == null || registrazioneForm.getRuolo() =="null" ) {
					FacesContext.getCurrentInstance().addMessage("registrazione:ruolo", 
							new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Seleziona un ruolo valido"));
					return false;
			}return true;
		}
	
	public boolean paginaValida() {
		String pass = registrazioneForm.getPassword();
		String repass = registrazioneForm.getRepassword();
		if(!repass.equals(pass)) {
			return false;
		}
		
		if(registrazioneForm.getEmail() == null || registrazioneForm.getEmail().equals(" ")) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "il campo e-mail non può essere vuoto");
            context.addMessage("registrazione:email", message);
            return false;
		}
		if(!verificaMail()){
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "inserisci una E-mail valida");
            context.addMessage("registrazione:email", message);
            return false;
            
		}if(emailPresente()) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "l'E-mail inserita è già presente in archivio");
            context.addMessage("registrazione:email", message);
            return false;
            
		}if(usernamePresente()) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "L'username inserito è già stato utilizzato");
            context.addMessage("registrazione:username", message);
            return false;
		}
		if(registrazioneForm.getRuolo().equals("null") || registrazioneForm.getRuolo().equals(null) ) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Seleziona un ruolo valido");
            context.addMessage("registrazione:ruolo", message);
            return false;
		}
			return true;
		
	}
	
	public RegistrazioneForm getRegistrazioneForm() {
		return registrazioneForm;
	}


	public void setRegistrazioneForm(RegistrazioneForm registrazioneForm) {
		this.registrazioneForm = registrazioneForm;
	}


	public RegistrazioneService getRegistrazioneService() {
		return registrazioneService;
	}


	public void setRegistrazioneService(RegistrazioneService registrazioneService) {
		this.registrazioneService = registrazioneService;
	}
	
	
	
	
}
