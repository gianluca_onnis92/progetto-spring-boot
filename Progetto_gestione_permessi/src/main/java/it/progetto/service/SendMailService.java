package it.progetto.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;


@Service
public class SendMailService  {

	@Autowired
	JavaMailSender javaMailSender;

	
    public SendMailService(JavaMailSender javaMailSender) {
		super();
		this.javaMailSender = javaMailSender;
    }
    
    public void inviaEmail( String to, String subject, String body) throws MessagingException{
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		
		helper = new MimeMessageHelper(message, true);
	
		helper.setSubject(subject);
		helper.setTo(to);
		helper.setText(body, true);
		javaMailSender.send(message);
    	 
		
	}


	public JavaMailSender getJavaMailSender() {
		return javaMailSender;
	}


	public void setJavaMailSender(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	public SendMailService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	
	
}
