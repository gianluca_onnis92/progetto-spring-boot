package it.progetto.service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.progetto.domain.Utente;
import it.progetto.repository.UtenteRepository;

@Service
public class RegistrazioneService {
	
	@Autowired
	UtenteRepository utenteRepository;	
	
	@Transactional
	public void registraUtente(Utente utente) {
		
		utenteRepository.save(utente);
	}



	public UtenteRepository getUtenteRepository() {
		return utenteRepository;
	}



	public void setUtenteRepository(UtenteRepository utenteRepository) {
		this.utenteRepository = utenteRepository;
	}
	
	public boolean emailPresente(String email) {
		List<Utente> listaUtenti = utenteRepository.findByEmail(email);
		return listaUtenti.size() > 0 ? true : false;
	}
	public boolean usernamePresente(String username) {
		List<Utente> listaUtenti = utenteRepository.findByUsername(username);
		return listaUtenti.size() > 0 ? true : false;
	}
	

}
