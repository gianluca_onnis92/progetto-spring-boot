package it.progetto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.progetto.domain.Permesso;
import it.progetto.repository.PermessoRepository;

@Service
public class PermessoService {

	@Autowired
	PermessoRepository permessoRepository;
	
	

	public Permesso trovaPermessoById_permesso(Long id) {
		Permesso p = permessoRepository.findOneByIdpermesso(id);
		return p;
	}
	
	@Transactional
	public void salvaPermesso(Permesso p) {
		permessoRepository.save(p);
	}

	public PermessoRepository getPermessoRepository() {
		return permessoRepository;
	}

	public void setPermessoRepository(PermessoRepository permessoRepository) {
		this.permessoRepository = permessoRepository;
	}
	
	
	
	
}
