package it.progetto.service;


import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import it.progetto.domain.Permesso;
import it.progetto.domain.Utente;
import it.progetto.repository.PermessoRepository;
import it.progetto.repository.UtenteRepository;


@Service
public class RichiestaService {
	
	
	
	@Autowired
	PermessoRepository permessoRepository;
	
	@Autowired
	UtenteRepository utenterepository;

	
	@Transactional
	public void registraPermesso(Permesso permesso) {	
		
		permessoRepository.save(permesso);
	}
	//lista di tutti gli utenti
	public ArrayList<Utente> listaUtenti(){
		ArrayList<Utente> listaUtenti =(ArrayList<Utente>) utenterepository.findAll();
		return listaUtenti;
	}
	//email utente
	public String email(Long id) {
		Utente u = utenterepository.findEmailByIdutente(id);
		String email = u.getEmail();
		return email;
	}

	//restituisce la lista di tutti i responsabili da selezionare nel menù a tendina 
	@Transactional
	public ArrayList<Utente> listaResponsabili() {
		 String ruolo="responsabile";
		 ArrayList<Utente> listaResponsabili =  Lists.newArrayList(utenterepository.findByRuolo(ruolo));
		 return listaResponsabili;
	}

	
	public ArrayList<Permesso> listaPermessi(){
		ArrayList<Permesso> listaPermessi =  Lists.newArrayList(permessoRepository.findAll());
		return listaPermessi;
	}

	//restituisce la lista di tutti i permessi richiesti dall'utente loggato
	public ArrayList<Permesso> listaPermessiUtente(){
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();	    
	    Utente id = utenterepository.findIdByUsername(name);	   
		Long u = id.getIdutente();
		ArrayList<Permesso> listaPermessi =  Lists.newArrayList(permessoRepository.findByUtenteAndTipo(u,true));
		
		return listaPermessi;
	}
	
	//restituisce la lista di tutti i permessi che sono stati concessi all'utente
	public ArrayList<Permesso> listaPermessiConcessi(){
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();	    
	    Utente id = utenterepository.findIdByUsername(name);	   
		Long u = id.getIdutente();
		String c= "si";
		ArrayList<Permesso> listaPermessi =  Lists.newArrayList(permessoRepository.findByUtenteAndTipoAndConcesso(u,true,c));
		
		return listaPermessi;
	}
	
	
	//restituisce la lista di tutte le richieste di ferie richieste dall'utente loggato
	public ArrayList<Permesso> listaFerieUtente(){
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();	    
	    Utente id = utenterepository.findIdByUsername(name);	   
		Long u = id.getIdutente();
		ArrayList<Permesso> listaFerie =  Lists.newArrayList(permessoRepository.findByUtenteAndTipo(u,false));
		
		return listaFerie;
	}
	
	//restituisce la lista di tutte le ferie che sono state concesse all'utente loggato
	public ArrayList<Permesso> listaFerieConcesse(){
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();	    
	    Utente id = utenterepository.findIdByUsername(name);	   
		Long u = id.getIdutente();
		String c ="si";
		ArrayList<Permesso> listaFerie =  Lists.newArrayList(permessoRepository.findByUtenteAndTipoAndConcesso(u,false,c));
		
		return listaFerie;
	}
	
	
	//restituise l'id dellutente connesso
	public Long IdUtenteConnesso() {
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();
	    
	   Utente id = utenterepository.findIdByUsername(name);
	    
	   
		return id.getIdutente();
	}

	

	//restituisce un unico oggetto utente relativo all'utente loggato
	public Utente UtenteConnesso() {
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();
	    
	   Utente loggato = utenterepository.findOneByUsername(name);
	    
	   
		return loggato;
	}
	
	//restituisce su stringa il numero delle richieste ricevute per l'account connesso(se esso è un rsponsabile)
	public String richieste() {
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();
	    String ruolo ="responsabile";
	    
	     
	    		if(utenterepository.findOneByUsernameAndRuolo(name, ruolo)!= null) {
	    			Long u = utenterepository.findOneByUsernameAndRuolo(name, ruolo).getIdutente();
	    			String s= String.valueOf(u);
	    			String c="in attesa";
	    		   	List<Permesso> richieste = permessoRepository.findAllByResponsabileAndConcesso(s,c);
	    		    	String str  =String.valueOf(richieste.size());	    
	    		    	return "Hai " + str + " nuove richieste";
	    		} 	
	    String str2 ="Area Responsabili";
	   return str2;
	    	
	}
	// true se l'utente connesso è un responsabile
	public boolean isResponsabile() {
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();
	    String ruolo ="responsabile";
	    if(utenterepository.findOneByUsernameAndRuolo(name, ruolo)!= null) {
	    	return true;
	    }
	    return false;
	}
	
	//lista delle richieste che ha ricevuto l'utente connesso e che puo convalidare
	public ArrayList<Permesso> richiesteRicevute(){		
		Authentication autenticato = SecurityContextHolder.getContext().getAuthentication();
	    String name = autenticato.getName();
	    Utente id = utenterepository.findIdByUsername(name);
	    String resp = String.valueOf(id.getIdutente());
	    String c ="in attesa";
	    ArrayList<Permesso> richieste = permessoRepository.findAllByResponsabileAndConcesso(resp, c );
	    return richieste;
	}
	
	
	public PermessoRepository getPermessoRepository() {
		return permessoRepository;
	}
	public void setPermessoRepository(PermessoRepository permessoRepository) {
		this.permessoRepository = permessoRepository;
	}
	public UtenteRepository getUtenterepository() {
		return utenterepository;
	}
	public void setUtenterepository(UtenteRepository utenterepository) {
		this.utenterepository = utenterepository;
	}

	
	
	
}
