package it.progetto.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import it.progetto.domain.Utente;
import it.progetto.repository.UtenteRepository;

@Component
public class GestionePermessiUserDetailsService implements UserDetailsService {
		
	@Autowired
	private UtenteRepository utenteRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		
		List<Utente> listaUtenti = utenteRepository.findByUsername(username);
				
		// si legge l'unico utente presente in base dati 
		Utente utente = listaUtenti.get(0);
	
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities = AuthorityUtils.createAuthorityList(utente.getIdutente().toString());
		
		// Create a UserDetails object from the data
		UserDetails userDetails = new org.springframework.security.core.userdetails.User(utente.getUsername(), utente.getPassword(),authorities);

		return userDetails;
	
	}
	




}

