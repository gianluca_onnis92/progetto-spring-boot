package it.progetto.gestione_permessi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import it.progetto.GestionePermessiApplication;


import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GestionePermessiApplication.class)
@WebAppConfiguration
public class GestionePermessiApplicationTests {

	@Test
	public void contextLoads() {
	}

}
